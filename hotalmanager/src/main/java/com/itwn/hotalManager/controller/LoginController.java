package com.itwn.hotalManager.controller;

import com.itwn.hotalManager.pojo.User;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.DisabledAccountException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class LoginController {
    @GetMapping("/login")
    public String test(){
        return "login";
    }

    @PostMapping("/login")
    public String login(){
        Subject subject = SecurityUtils.getSubject();
        if(subject.isAuthenticated()){
            return "success";
        }
        return "login";
    }
    @RequestMapping("/login/check")
    public String login(String username,String password, Model model){
        String status = "0";
        System.out.println(1);
        System.out.println(username);
        Subject subject = SecurityUtils.getSubject();
        UsernamePasswordToken token = new UsernamePasswordToken(username,password);
        try {
            subject.login(token);
            status="1";
            model.addAttribute("message", "登录成功");
            return "success";
        }catch (UnknownAccountException e1){
            model.addAttribute("message", "用户名错误");
        }catch (DisabledAccountException e2){
            model.addAttribute("message", "用户名未启用");
        }catch (IncorrectCredentialsException e3){
            model.addAttribute("message", "密码错误");
        }catch (Throwable e){
            model.addAttribute("message", "其他错误");
        }
        return status;
    }

    @RequestMapping("/logout")
    public String logout(){
        SecurityUtils.getSubject().logout();
        return "login";
    }
}
