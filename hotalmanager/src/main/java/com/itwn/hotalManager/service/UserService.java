package com.itwn.hotalManager.service;

import com.itwn.hotalManager.pojo.User;

public interface UserService {
    User queryUserByName(String username);
}
