package com.itwn.hotalManager.service;

import java.util.List;

public interface PermissionService {
    List<String> queryPermissionByUsername(String username);
}
