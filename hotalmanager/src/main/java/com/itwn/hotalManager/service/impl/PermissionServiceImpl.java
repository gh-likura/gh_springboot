package com.itwn.hotalManager.service.impl;

import com.itwn.hotalManager.Mapper.PermissionMapper;
import com.itwn.hotalManager.service.PermissionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class PermissionServiceImpl implements PermissionService {
    @Autowired
    private PermissionMapper permissionMapper;
    @Override
    public List<String> queryPermissionByUsername(String userName) {
        return permissionMapper.queryPermissionByUsername(userName);
    }
}
