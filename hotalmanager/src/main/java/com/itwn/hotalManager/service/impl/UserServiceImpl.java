package com.itwn.hotalManager.service.impl;

import com.itwn.hotalManager.Mapper.UserMapper;
import com.itwn.hotalManager.pojo.User;
import com.itwn.hotalManager.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    @Override
    public User queryUserByName(String username) {
        System.out.println("serviceImpl"+username);
        return userMapper.queryUserByName(username);
    }
}
