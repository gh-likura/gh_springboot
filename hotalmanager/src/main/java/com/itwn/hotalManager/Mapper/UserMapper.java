package com.itwn.hotalManager.Mapper;

import com.itwn.hotalManager.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface UserMapper extends tk.mybatis.mapper.common.Mapper<User> {
    @Select("select * from sys_user where username='${username}'")
    public User queryUserByName(@Param("username") String username);
}
