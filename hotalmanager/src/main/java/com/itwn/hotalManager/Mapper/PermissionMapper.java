package com.itwn.hotalManager.Mapper;

import com.itwn.hotalManager.pojo.Permission;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface PermissionMapper extends tk.mybatis.mapper.common.Mapper<Permission> {
    @Select("SELECT" +
            " sys_permission.`permission`" +
            " FROM sys_permission" +
            " INNER JOIN sys_role_permission" +
            " ON sys_permission.`id`=sys_role_permission.`permission_id`" +
            " INNER JOIN sys_role" +
            " ON sys_role.`role_id`=sys_role_permission.`role_id`" +
            " INNER JOIN sys_user_role" +
            " ON sys_user_role.`role_id`=sys_role.`role_id`" +
            " INNER JOIN sys_user" +
            " ON sys_user.id=sys_user_role.`user_id`" +
            " WHERE sys_user.`username`='${username}'")
    public List<String> queryPermissionByUsername(@Param("username") String username);
}
