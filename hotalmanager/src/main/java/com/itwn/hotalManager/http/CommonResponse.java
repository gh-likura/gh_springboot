package com.itwn.hotalManager.http;

public class CommonResponse<T> {
    /*
    * 200  操作成功
    * 301  302重定向
    * 400
    * 401 未授权
    * 500 服务端内部错误
    * */
    /*服务端响应的代码*/
    private Integer code;
    //服务端返回的错误消息描述
    private String message;
    //服务端返回的真实数据
    private T data;

    public CommonResponse(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }
    public CommonResponse(Integer code, String message) {
        this.code = code;
        this.message = message;
        this.data = null;
    }
    public CommonResponse(T data) {
        this.code = 200;
        this.message = "操作成功";
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "CommonResponse{" +
                "code=" + code +
                ", message='" + message + '\'' +
                ", data=" + data +
                '}';
    }
}
