package com.itwn.hotalManager.global;


import com.itwn.hotalManager.http.CommonResponse;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

//@ControllerAdvice
public class GlobalExceptionHandel {
    @ExceptionHandler(ArrayIndexOutOfBoundsException.class)
    @ResponseBody
    public CommonResponse  exceptionProcessor(ArrayIndexOutOfBoundsException e){
        CommonResponse<Void> response = new CommonResponse<Void>(400,"数值越界");
        return response;
    }
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public CommonResponse  exceptionProcessor(Exception e){
        CommonResponse<Void> response = new CommonResponse<Void>(500,"服务端代码异常");
        return response;
    }
    @ExceptionHandler(Throwable.class)
    @ResponseBody
    public CommonResponse  exceptionProcessor(Throwable e){
        CommonResponse<Void> response = new CommonResponse<Void>(400,"客户端错误");
        return response;
    }
 }
