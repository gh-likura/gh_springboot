package com.itwn.hotalManager.shiro;

import com.itwn.hotalManager.pojo.User;
import com.itwn.hotalManager.service.PermissionService;
import com.itwn.hotalManager.service.UserService;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.util.ByteSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class MyRealm extends AuthorizingRealm {
    private final static Logger USER_LOGIN_LOGGER = LoggerFactory.getLogger(MyRealm.class);
    @Autowired
    private UserService userService;
    @Autowired
    private PermissionService permissionService;




    /**
     * @Description 授权   登陆之后的用户权限  判断当下用户有没有执行方法的权限
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        User user = (User) principalCollection.getPrimaryPrincipal();
        List<String> permission = permissionService.queryPermissionByUsername(user.getUsername());
        System.out.println("权限："+permission);
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        info.addStringPermissions(permission);
        USER_LOGIN_LOGGER.info("doGetAuthorizationInfo");
        return info;
    }
    /**
     * 认证   用户登录
     *
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        UsernamePasswordToken token = (UsernamePasswordToken) authenticationToken;
        User user = userService.queryUserByName(token.getUsername());
        System.out.println(user);
        if (user == null) {
            return null;
        }
        List<String> permission = permissionService.queryPermissionByUsername(user.getUsername());
        System.out.println("权限："+permission);
        USER_LOGIN_LOGGER.info("doGetAuthenticationInfo");
        SimpleAuthenticationInfo info = new SimpleAuthenticationInfo(user, user.getPassword().toCharArray(), ByteSource.Util.bytes(user.getSalt()), getName());
        return info;
    }
}
